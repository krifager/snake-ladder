package GUI;

import javax.swing.*;
import java.awt.*;
import java.util.Random;
import Handling.*;

public class SnakeLadderGameGUI {
    //GLOBAL STRINGS AND INTS
    private final String[] namesArr;
    private final String[] colorsArr;
    private final String[] diceLabelText = new String[3];//
    private final int X = 6, Y = 6; //6 * 6 is 36. 36 places for the players to be at
    private final int PLAYERS = 2;// PUT IN CONFIG FILE?
    private final int[] playerPosition = new int[PLAYERS + 1];//counts how far the player is
    private final int[] snakeOrStairs;//LOOK TO CLASS PutSnakeOrStairs AT BOTTOM
    private int latestRoll = 0;
    private int playerTurn = 1;
    private boolean pieceCanMove = false;

    //GUI GLOBALS
    private final JPanel[] spotPanels = new JPanel[X * Y +1];//panels inside each spot on grid
    private final JLabel[] rollSum = new JLabel[3];//label used for showing your number
    private final JLabel playerTurnLabel = new JLabel();//Text that shows turn it is
    private final JLabel winnerwinnerchickendinner = new JLabel("YE BITCH U WON");
    private final JButton[] playerButtons = new JButton[PLAYERS + 1];//the players "brick"
    private final JButton[][] spots = new JButton[X + 1][Y + 1];//spots on grid
    private final JButton roll = new JButton("Roll"); //roll button

    //gets the frame to start game at, the colors and names of the players
    public SnakeLadderGameGUI(JFrame f, String[] colors, String[] names){
        namesArr = names; colorsArr = colors;
        playerPosition[1] = 1; playerPosition[2] = 1;//both players start at spot 1

        //adds menu bar on top of frame
        GUIHandling getMenuBar = new GUIHandling();
        getMenuBar.menuBar(f);

        //puts every spot from 1 - 36 on grid
        JPanel p = initGameSpots(f);

        initResignButton(f,p);
        initRollButton(f,p);
        initPlayerButtons();
        initPlayersTurn(p);
        initRestartButton(f,p);

        //get array which tells if a square/spot on the grid is connected to a snake or stair
        SnakeLadderGameGUI.PutStairsAndSnakes temp = new SnakeLadderGameGUI.PutStairsAndSnakes(X,Y);
        snakeOrStairs=temp.getSnakeOrStair(spots);
        p.repaint();p.revalidate();

        //layered pane
        /*ImageIcon stair = new ImageIcon((new ImageIcon("/home/prog/IdeaProjects/Game/src/GUI/stair.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT)));
        JLabel l = new JLabel(); l.setIcon(stair);
        GridBagConstraints gbc = new GridBagConstraints();gbc.gridx=40;gbc.gridy=40;
        p.add(l,gbc);*/
        f.setVisible(true);
    }

    //sets players button color
    private void initPlayerButtons(){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,0,0,0);//top space

        //give player buttons color and name of player
        for(int i = 1; i <= PLAYERS; i++){
            JButton b = new JButton(namesArr[i]);//button with player name on it
            playerButtons[i] = b;//put button in global array
            Dimension bSize = b.getPreferredSize();
            bSize.width = 10; bSize.height = 10;
            b.setPreferredSize(bSize);

            //colorsArr has the players wanted colors
            if(colorsArr[i].equals("YELLOW")){
                playerButtons[i].setBackground(Color.YELLOW);
            } else if(colorsArr[i].equals("BLUE")){
                playerButtons[i].setBackground(Color.BLUE);
            } else {
                playerButtons[i].setBackground(Color.RED);
            }
        }

        //PLACE PLAYER BUTTONS INSIDE PANELS.(Each spot has a panel inside it, which can reside buttons)
        for(int i = 1; i <= PLAYERS; i++){
            gbc.gridx=0;gbc.gridy=(i-1);spotPanels[1].add(playerButtons[i],gbc);//ADDS BUTTON TO PANEL
            spotPanels[1].setVisible(true);
        }
    }
    //automatically sets the last player to be first
    public void initPlayersTurn(JPanel p){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(0,0,0,0);

        playerTurnLabel.setText(namesArr[1]+"s turn");//BUG: THIS WONT WORK IF THERE ARE MORE THAN TWO PLAYERS
        gbc.gridx=(X + 1);gbc.gridy=(Y+1);p.add(playerTurnLabel);
    }
    //roll dice button to the bottom right
    private void initRollButton(JFrame f, JPanel p) {
        for(int i = 1; i <= 2; i++){
            JLabel l = new JLabel();
            rollSum[i]=l;
        }
        roll.addActionListener(e -> setRollButtonAction());
        //place below spots
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,10,0,0);//top space
        gbc.gridx= X +1;gbc.gridy= Y +2;p.add(roll,gbc);//adds button one spot bellow and to the right
        gbc.gridx= X +2;gbc.gridy= Y +2;p.add(rollSum[1],gbc);gbc.gridx= X +3;gbc.gridy= Y +2;p.add(rollSum[2],gbc);
        f.getContentPane().add(p);
    }
    //put resign button on to grid
    public void initResignButton(JFrame f, JPanel p) {
        JButton resign = new JButton("Resign");
        resign.addActionListener(e -> setResignButtonAction(p));
        //place below spots
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,0,0,0);//top space
        gbc.gridx= X +1;gbc.gridy= Y +1;p.add(resign,gbc);//adds button one spot bellow and to the right
        f.getContentPane().add(p);
    }
    //restart the game
    public void initRestartButton(JFrame f, JPanel p){
        JButton resign = new JButton("Restart");
        resign.addActionListener(e -> setRestartButtonAction(f));
        //place below spots
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,0,0,0);//top space
        gbc.gridx= X +1;gbc.gridy= Y +4;p.add(resign,gbc);//adds button one spot bellow and to the right
        f.getContentPane().add(p);
    }
    //sets spots on frame in random colors and gives them action listeners for when the players want to move
    public JPanel initGameSpots(JFrame f){
        int spotWidth = 50, spotHeight = 50;//height and width of spots

        //CREATE PANEL AND GRID TO PLACE BUTTONS/SPOTS
        JPanel p = new JPanel(new GridBagLayout());//panel with grid to put buttons on
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(0,0,0,0);//no space between spots

        //Give spot a random color --> GRAY, GREEN, MAGENTA
        Random rand = new Random();

        //Go through grid and each spot and give random colors
        //also sets action listener to all spots
        //also changes the text in top right of the screen which says whose turn it is
        int counter = 0;
        for(int j = Y; j >= 1; j--){
            for(int i = 1; i <= X; i++){
                int colorNr = rand.nextInt(3) + 1;
                JButton b1 = new JButton();
                Dimension b1Size = b1.getPreferredSize();
                b1Size.width = spotWidth; b1Size.height = spotHeight;
                b1.setPreferredSize(b1Size);
                ++counter;//counter
                b1.setText(Integer.toString(counter));//buttons nr


                //SET A PANEL INSIDE THE SPOT FOR THE PLAY BUTTONS TO BE ADDED ON TO
                spotPanels[counter] = new JPanel(new GridBagLayout());
                b1.add(spotPanels[counter]); spotPanels[counter].setVisible(false);

                spots[i][j] = b1;
                if(colorNr == 1){       //SET BUTTON GREY
                    spotPanels[counter].setBackground(Color.PINK);//make semi-transparent
                    b1.setBackground(Color.PINK);
                } else if(colorNr == 2){//SET BUTTON GREEN
                    spotPanels[counter].setBackground(Color.GREEN);
                    b1.setBackground(Color.GREEN);
                } else {                //SET BUTTON MAGENTA
                    spotPanels[counter].setBackground(Color.MAGENTA);
                    b1.setBackground(Color.magenta);
                }

                int spotNr = Integer.parseInt(spots[i][j].getText());//spot for the players piece to go to
                spots[i][j].addActionListener(e -> {
                    //EventQueue.invokeLater(()->setGameSpotsAction(f,p,spotNr));
                    setGameSpotsAction(p,spotNr);
                });
                gbc.gridx=i;gbc.gridy=j;p.add(b1,gbc);
            }
        }
        f.getContentPane().add(p);
        return p;
    }
    //CLASS OPENS A SEPARATE FRAME TO REGISTER THE TWO PLAYERS
    public static class InitPlayers {
        private final int nrOfColors = 3; //red, blue, yellow
        private JRadioButton[][] colorsBUTTONS;
        private JTextField[] namesTEXTBOX;
        private final int PLAYERS = 2;// PUT IN CONFIG FILE?
        private final String[] names = new String[PLAYERS + 1];
        private final String[] colors = new String[PLAYERS + 1];



        public void putPlayers(JFrame f){
            //A player should be able to enter his/her name before the game starts.
            //A player can choose between different colors/types of tokens (pieces) i.e., a basic pawn shape (red/blue/green/yellow), a car, a horse.
            JPanel p = new JPanel(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(10,10,10,10);
            //setting array length
            colorsBUTTONS = new JRadioButton[PLAYERS + 1][nrOfColors + 1];
            namesTEXTBOX = new JTextField[PLAYERS + 1];

            for(int i = 1; i <= PLAYERS; i++){
                names[i] = "";
                colors[i] = "";
            }
            //variable j is used to place components on the grid in "y" direction
            int j = 1;
            for(int i = 1; i <= PLAYERS; i++) {
                addPlayerGUI(p,i,gbc);
                ++j;
            }

            //WHEN BEGIN BUTTON IS PRESSED GAME WILL START
            JButton begin = new JButton("Begin game");
            gbc.gridx=0;gbc.gridy=j;p.add(begin,gbc);
            begin.addActionListener(e -> {
                boolean check;
                check = getPlayerValues();
                if(!check) {
                    f.dispose();//exiting this frame
                    //new basic frame for the actual game
                    GUIHandling newFrame = new GUIHandling();
                    JFrame frame = newFrame.initGUI();
                    //ACTUAL GAME GUI
                    new SnakeLadderGameGUI(frame, colors, names);
                }

            });
            f.getContentPane().add(p);
            f.setVisible(true);
        }

        private void addPlayerGUI(JPanel p, int i, GridBagConstraints gbc){
            int k = 0;
            String player = "Name player " + i + ": ";
            JLabel name = new JLabel(player); JTextField text = new JTextField();
            Dimension textSize = text.getPreferredSize();
            textSize.width = 100; text.setPreferredSize(textSize);

            JRadioButton blue, red, yellow;
            blue = new JRadioButton("Blue");red = new JRadioButton("Red");yellow = new JRadioButton("Yellow");
            ButtonGroup group = new ButtonGroup();
            group.add(red);group.add(blue);group.add(yellow);

            gbc.gridx=k;gbc.gridy= i;p.add(name,gbc);gbc.gridx=++k;gbc.gridy= i;p.add(text,gbc);
            gbc.gridx=++k;gbc.gridy= i;p.add(blue,gbc);gbc.gridx=++k;gbc.gridy= i;p.add(red,gbc);gbc.gridx=++k;gbc.gridy= i;p.add(yellow,gbc);

            //setting text fields and button values in array
            namesTEXTBOX[i] = text;
            colorsBUTTONS[i][1] = blue;colorsBUTTONS[i][2] = red;colorsBUTTONS[i][3] = yellow;
        }

        private boolean getPlayerValues(){
            //go through text fields and get their values
            int i = 1, k=0;
            while(i <= PLAYERS) {
                names[i] = namesTEXTBOX[i].getText();
                if(names[i].length() != 0)
                    ++k;
                ++i;
            }
            int checkNames = k, checkButtons = 0;


            //go through all button groups
            for(i = 1; i <= PLAYERS; i++){
                //go through all buttons in button group
                for(int j = 1; j <= nrOfColors; j++){//maybe a while loop, but wont affect much
                    if(colorsBUTTONS[i][j].isSelected()){
                        ++checkButtons;
                        colors[i] = colorsBUTTONS[i][j].getText().toUpperCase();//get the color(type string)
                    }
                }
            }

            return !(checkNames == PLAYERS && checkButtons == PLAYERS);

        }
    }

    //show the winner of the dinner
    public void setResignButtonAction(JPanel p){
        changePlayerTurn("1","2");
        p.removeAll();
        winnerwinnerchickendinner.setText(namesArr[playerTurn]+" WON!!!!!!");
        GridBagConstraints gbc = new GridBagConstraints(); gbc.gridx=0;gbc.gridy=0;
        p.add(winnerwinnerchickendinner,gbc);
        p.repaint();p.revalidate();
    }
    public void setRestartButtonAction(JFrame f){
        f.dispose();
        GUIHandling newFrame = new GUIHandling();
        JFrame frame = newFrame.initGUI();
        frame.setTitle("Snake ladder game");

        new SnakeLadderGameGUI(frame, colorsArr, namesArr);
    }
    //action to do when rolling dice
    public void setRollButtonAction(){
        //roll twice, and put result on screen
        diceLabelText[1] = playerRolls();
        rollSum[1].setText(diceLabelText[1]);
        diceLabelText[2] = playerRolls();
        rollSum[2].setText(diceLabelText[2]);//set text on label

        roll.setEnabled(false);//make roll button NOT enabled
        latestRoll = Integer.parseInt(diceLabelText[1]) + Integer.parseInt(diceLabelText[2]);
        pieceCanMove = true;
    }
    //action to do when a spot/square is pressed
    public void setGameSpotsAction(JPanel p, int nr) {//nr is the spot where the piece should go
        if(pieceCanMove) {
            if (nr == X * Y && playerPosition[playerTurn] + latestRoll == nr) {//if dice is rolled
                p.removeAll();
                winnerwinnerchickendinner.setText(namesArr[playerTurn] + " WON!!!!!!");
                GridBagConstraints gbc = new GridBagConstraints();
                gbc.gridx = 0;
                gbc.gridy = 0;
                p.add(winnerwinnerchickendinner, gbc);
                p.repaint();
                p.revalidate();
            } else if (latestRoll >= 2 && nr <= X * Y && playerPosition[playerTurn] + latestRoll == nr) {//
                int sot = snakeOrStairs[playerPosition[playerTurn] + latestRoll];//sot stands for Snake Or sTair
                //if just regular square/spot
                if (playerPosition[1] != playerPosition[2]) {//if player moves and the previous spot is empty, make panel invisible.
                    spotPanels[playerPosition[playerTurn]].setVisible(false);
                }
                if (sot == 0) {
                    playerPosition[playerTurn] += latestRoll;//button has new position
                    movePlayerButton(nr);
                } else if (sot > 0) {//if positive number, we can go up!!
                    playerPosition[playerTurn] += latestRoll + sot;//button has new position
                    movePlayerButton(nr + sot);
                } else {//god damn it we going down
                    playerPosition[playerTurn] += latestRoll - sot;//button has new position
                    movePlayerButton(nr - sot);
                }
                changePlayerTurn(diceLabelText[1], diceLabelText[2]);
                roll.setEnabled(true);//next player can now roll
                pieceCanMove = false;//piece is not allowed to move until rolled again
            } else if(nr > X*Y){
                if(rollSum[1] != rollSum[2]) {
                    changePlayerTurn("1","2");//just give it 1 and 2 so that it will always change players turn
                }
                roll.setEnabled(true);//make player button available
            }

        }

    }

    //DOES NOT WORK WITH MULTIPLE PLAYERS, ONLY TWO
    public void changePlayerTurn(String text1, String text2){
        if(!text1.equals(text2)){
            if(playerTurn == 1){
                ++playerTurn;
            }else{
                --playerTurn;
            }
            setPlayersTurnLabel();
        }
    }

    //returns string to put on label
    public String playerRolls(){
        RollADice dice = new RollADice();
        latestRoll = dice.rollTheDice();
        return Integer.toString(latestRoll);
    }


    public void movePlayerButton(int spotNr){
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx=0;gbc.gridy=playerTurn-1;
        spotPanels[spotNr].add(playerButtons[playerTurn],gbc);//move players button to the new spot
        spotPanels[spotNr].setVisible(true);//set the panel to visible
        spotPanels[spotNr - latestRoll].repaint();spotPanels[spotNr - latestRoll].revalidate();
    }
    public void setPlayersTurnLabel(){
        int i = 1;
        while( i != playerTurn){
            ++i;
        }
        playerTurnLabel.setText(namesArr[i]+"s turn");
    }

    //CLASS MAKES STAIRS AND SNAKES
    public static class PutStairsAndSnakes{

        private final int[] snakeOrStair;//zero means no snake or stair. Its just a regular spot
                                                    // If negative number, snake.
                                                    //      EXAMPLE: [0,0...,-7...,0,0]
                                                    //              this means the player should fall 7 places back
                                                    //If positive number, stair
                                                    //      EXAMPLE: [0,0...,9...,0,0]
                                                    //              this means the player should move 9 places forward
        ImageIcon stairImg, snakeImg;


        public PutStairsAndSnakes(int x, int y){
            snakeOrStair = new int[x * y +1];


            //SHOULD PROBABLY HAVE SOME CHECKING IF THEY EXIST OR NOT
            //get stair image
            //String localDir = System.clearProperty("user.dir");
            //stairImg = new ImageIcon("/home/prog/IdeaProjects/Game/src/GUI/stair.png");
            //stairImg = new ImageIcon(new ImageIcon(localDir+"/src/GUI/stair.png").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
            //snake img
            //getClass().getClassLoader().getResource("/filepath/filename");
            stairImg = new ImageIcon(new ImageIcon(this.getClass().getClassLoader().getResource("").getPath() + "src/GUI/stair.png").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));

            //snakeImg = new ImageIcon("/home/prog/IdeaProjects/Game/src/GUI/stair.png");
            //snakeImg = new ImageIcon(new ImageIcon(localDir+"/src/GUI/snake.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
            snakeImg = new ImageIcon(new ImageIcon(this.getClass().getClassLoader().getResource("").getPath() + "src/GUI/snake.jpg").getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));



            //initialize "snakeOrStairs[]" to zeroes
            for(int i = 1; i <= x * y; i++){
                snakeOrStair[i]=0;
            }
        }

        //can build this more complicated. But im not getting paid enough
        public void makeSnakes(JButton[][] spots){
            //there MUST be a snake at spot 35
            snakeOrStair[35] = -6;//fall one place down to spot 29
            spots[5][1].setIcon(snakeImg);
            spots[5][2].setIcon(snakeImg);

            snakeOrStair[22] = -18;//fall to spot 4
            spots[4][3].setIcon(snakeImg);
            spots[4][4].setIcon(snakeImg);
            spots[4][5].setIcon(snakeImg);
            spots[4][6].setIcon(snakeImg);
        }

        //can build this more complicated. But im not getting paid enough
        public void makeStairs(JButton[][] spots){

            snakeOrStair[6]=6;//up to spot 12
            spots[6][6].setIcon(stairImg);
            spots[6][5].setIcon(stairImg);

            snakeOrStair[21]=12;//up to spot 33
            spots[3][3].setIcon(stairImg);
            spots[3][2].setIcon(stairImg);
            spots[3][1].setIcon(stairImg);
        }

        public int[] getSnakeOrStair(JButton[][] spots){
            makeSnakes(spots);makeStairs(spots);
            return snakeOrStair;
        }

    }
}

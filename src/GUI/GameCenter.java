package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Handling.*;

public class GameCenter {
    public GameCenter(){
        //get new basic frame
        GUIHandling newFrame = new GUIHandling();
        JFrame f = newFrame.initGUI();

        //adding buttons for the two games
        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,10,10,10);

        JButton rollADice = new JButton("Roll a dice");
        rollADice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RollADiceGUI();
            }
        });

        JButton maxClicks = new JButton("Maximum clicks");
        maxClicks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MaximumClicks();
            }
        });

        JButton task3 = new JButton("Snake ladder game");
        task3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SnakeLadderGame();
            }
        });

        JButton exitButton = addExitButton(f);

        gbc.gridx=0;gbc.gridy=0;p.add(rollADice,gbc);
        gbc.gridx=1;gbc.gridy=0;p.add(maxClicks,gbc);
        gbc.gridx=2;gbc.gridy=0;p.add(task3,gbc);
        gbc.gridx=3;gbc.gridy=2;p.add(exitButton,gbc);

        f.getContentPane().add(p);
        f.setVisible(true);
    }
    public JButton addExitButton(JFrame f){
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });
        return exitButton;
    }
}

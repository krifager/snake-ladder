package GUI;

import javax.swing.*;

public class GUIHandling {

    public JFrame initGUI(){
        JFrame f = new JFrame("Game");
        f.setSize(1000,750);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        return f;
    }
    public void menuBar(JFrame f){
        JMenuBar mb = new JMenuBar();

        //FILE MENU
        JMenu menu = new JMenu("File");
        JMenuItem newGame, reset, resign, exit;
        newGame = new JMenuItem("New game");
        reset = new JMenuItem("Reset");
        resign = new JMenuItem("Resign");
        exit = new JMenuItem("Exit");
        menu.add(newGame);menu.add(reset);menu.add(resign);menu.add(exit);

        //HELP MENU
        JMenu help = new JMenu("Help");
        JMenuItem howToPlay;
        howToPlay = new JMenuItem("How to play / Instructions");
        help.add(howToPlay);

        //ABOUT MENU
        JMenu about = new JMenu("About");
        JMenuItem gameVersion, author;
        gameVersion = new JMenuItem("Game version");
        author = new JMenuItem("Author");
        about.add(gameVersion);about.add(author);

        mb.add(menu);mb.add(help);mb.add(about);
        f.setJMenuBar(mb);
    }
}

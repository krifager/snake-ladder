package Handling;

import GUI.GUIHandling;
import GUI.SnakeLadderGameGUI;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SnakeLadderGame {

    public SnakeLadderGame(){
        //new basic frame for game
        makeSnakeLadderGUI();
    }
    public void makeSnakeLadderGUI(){
        GUIHandling newFrame = new GUIHandling();
        JFrame f = newFrame.initGUI();
        f.setTitle("Snake ladder game");

        //"InitPlayers.putPlayers(f)" gives players name and colors,
        // but also starts the game after init of players are done
        SnakeLadderGameGUI.InitPlayers addPlayersOnGUI = new SnakeLadderGameGUI.InitPlayers();
        addPlayersOnGUI.putPlayers(f);
    }


}

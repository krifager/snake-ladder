package Handling;

import java.util.Random;
public class RollADice {
    public int rollTheDice() {
        Random rand = new Random();
        return rand.nextInt(6) + 1;
    }
}

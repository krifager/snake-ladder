package Handling;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;

public class GUIHandling {

    public GUIHandling(){
        JFrame f = initGUI();
        GameCenter(f);
    }
    //initial window
    public JFrame initGUI(){
        JFrame f = new JFrame("Game");
        f.setSize(1000,750);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        return f;
    }

    //Primary window - adding button for the two games and the exit button
    public void GameCenter(JFrame f){

        //adding buttons for the two games
        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,10,10,10);

        JButton rollADice = new JButton("Roll a dice");
        rollADice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //GUIRollADice();
            }
        });

        JButton maxClicks = new JButton("Maximum clicks");
        maxClicks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                maximumClicks();
            }
        });

        JButton task3 = new JButton("Snake ladder game");
        task3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SnakeGameGUI();
            }
        });

        JButton exitButton = addExitButton(f);

        gbc.gridx=0;gbc.gridy=0;p.add(rollADice,gbc);
        gbc.gridx=1;gbc.gridy=0;p.add(maxClicks,gbc);
        gbc.gridx=2;gbc.gridy=0;p.add(task3,gbc);
        gbc.gridx=3;gbc.gridy=2;p.add(exitButton,gbc);

        f.getContentPane().add(p);
        f.setVisible(true);
    }


    public JButton addExitButton(JFrame f){
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });
        return exitButton;
    }


    //new window for game
    public void maximumClicks(){
        JFrame f = initGUI();
        f.setTitle("Maximum clicks");


        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,10,10,10);

        JLabel text = new JLabel("See how many clicks you can do in a minute!");
        JLabel record = new JLabel("Record: ");
        //GET THE RECORD HERE
        JButton start = new JButton("Start");
        JButton clickMe = new JButton("CLICK ME!");
        JLabel lblNumber = new JLabel();
        JLabel timeLeft = new JLabel("Time left: ");
        JLabel seconds = new JLabel(" seconds");

        //when mouse is clicked update the label
        clickMe.addMouseListener(new MouseAdapter() {
            int count = 0;
            @Override
            public void mouseClicked(MouseEvent e) {
                count++;
                lblNumber.setText(Integer.toString(count));
            }
        });

        gbc.gridx=0;gbc.gridy=0;p.add(text,gbc);
        gbc.gridx=0;gbc.gridy=1;p.add(record,gbc);
        gbc.gridx=0;gbc.gridy=2;p.add(start,gbc);
        gbc.gridx=0;gbc.gridy=3;p.add(clickMe,gbc);
        gbc.gridx=1;gbc.gridy=3;p.add(lblNumber,gbc);
        gbc.gridx=0;gbc.gridy=4;p.add(timeLeft,gbc);
        //countdown class that puts the countdown on the panel
        CountDown countdown = new CountDown(p, gbc,start);
        gbc.gridx=2;gbc.gridy=4;p.add(seconds,gbc);

        f.getContentPane().add(p);
        f.setVisible(true);
    }

    public void SnakeGameGUI(){
        //gives a basic GUI window
        JFrame f = initGUI();
        f.setTitle("Snake Ladder Game");
        //Asks players for name and their piece color
        initTask3(f);
        //adds menu bar on top and their functionality
        menuBar(f);
        f.setVisible(true);
    }

    public void initTask3(JFrame f){
        //A player should be able to enter his/her name before the game starts.
        //A player can choose between different colors/types of tokens (pieces) i.e., a basic pawn shape (red/blue/green/yellow), a car, a horse.
        int PLAYERS = 2;
        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10,10,10,10);

        int j = 1;
        for(int i = 1; i <= PLAYERS; i++) {
            addPlayerGUI(p,i,gbc);
            ++j;
        }
        JButton begin = new JButton("Begin game");
        gbc.gridx=0;gbc.gridy=j;p.add(begin,gbc);
        f.getContentPane().add(p);

    }

    public void addPlayerGUI(JPanel p, int i, GridBagConstraints gbc){
        int j = i,k = 0;
        String player = "Name player " + i + ": ";
        JLabel name = new JLabel(player); JTextField text = new JTextField();
        Dimension textSize = text.getPreferredSize();
        textSize.width = 100; text.setPreferredSize(textSize);

        JRadioButton blue, red, yellow;
        blue = new JRadioButton("Blue");red = new JRadioButton("Red");yellow = new JRadioButton("Yellow");
        ButtonGroup group = new ButtonGroup();
        group.add(red);group.add(blue);group.add(yellow);

        gbc.gridx=k;gbc.gridy=j;p.add(name,gbc);gbc.gridx=++k;gbc.gridy=j;p.add(text,gbc);
        gbc.gridx=++k;gbc.gridy=j;p.add(blue,gbc);gbc.gridx=++k;gbc.gridy=j;p.add(red,gbc);gbc.gridx=++k;gbc.gridy=j;p.add(yellow,gbc);
    }
    public void menuBar(JFrame f){
        JMenuBar mb = new JMenuBar();

        //FILE MENU
        JMenu menu = new JMenu("File");
        JMenuItem newGame, reset, resign, exit;
        newGame = new JMenuItem("New game");
        reset = new JMenuItem("Reset");
        resign = new JMenuItem("Resign");
        exit = new JMenuItem("Exit");
        menu.add(newGame);menu.add(reset);menu.add(resign);menu.add(exit);

        //HELP MENU
        JMenu help = new JMenu("Help");
        JMenuItem howToPlay;
        howToPlay = new JMenuItem("How to play / Instructions");
        help.add(howToPlay);

        //ABOUT MENU
        JMenu about = new JMenu("About");
        JMenuItem gameVersion, author;
        gameVersion = new JMenuItem("Game version");
        author = new JMenuItem("Author");
        about.add(gameVersion);about.add(author);

        mb.add(menu);mb.add(help);mb.add(about);
        f.setJMenuBar(mb);
    }


    //https://stackoverflow.com/a/28338476/11155582
    public class CountDown {
        private Timer timer;
        private long startTime = -1;
        private long duration = 60000;

        private JLabel label;

        public CountDown(JPanel p, GridBagConstraints gbc, JButton b) {
            timer = new Timer(10, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (startTime < 0) {
                        startTime = System.currentTimeMillis();
                    }
                    long now = System.currentTimeMillis();
                    long clockTime = now - startTime;
                    if (clockTime >= duration) {
                        clockTime = duration;
                        timer.stop();
                    }
                    SimpleDateFormat df = new SimpleDateFormat("mm:ss:SSS");
                    label.setText(df.format(duration - clockTime));
                }
            });
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    timer.start();
                }
            });
            label = new JLabel("...");
            gbc.gridx=1;gbc.gridy=4;p.add(label,gbc);
        }

    }
}
